# html-css


## Exercice Sélecteurs CSS [html](exo-selector.html)/[css](css/exo-selector.css)
1. Créer un dossier css avec un fichier exo-selector.css et le lier au fichier html, puis dans le fichier exo-selector.html...
2. ...créer une balise section avec un id "first" qui contiendra un h1 avec First Section dedans
3. Dans la section créer un paragraphe avec "some text" dedans qui aura également une balise span avec une classe "highlight"  et comme contenu " and important text"
4. Dans le fichier CSS sélectionner le h1 à l'intérieur de la section first et mettre sa couleur en bleu et le mettre en gras
5. Ensuite, sélectionner la classe highlight et mettre sa couleur de fond en jaune et mettre son texte en italique
6. Dans le HTML, créer un nouveau paragraphe et lui mettre la  classe highlight et comme contenu "another paragraph", ainsi qu'un autre span avec la classe highlight dessus et " with more important text" comme contenu
7. Dans le css, faire une règle pour que les classe highlight à l'intérieur d'une classe highlight ait leur texte en bleu
8. Créer une nouvelle règle css qui mettra une bordure en pointillés autour des paragraphes qui sont dans un article (pour fêter ça,  faire un article avec un paragraphe dedans)
9. Mettre un h2 dans l'article puis faire une règle qui change la taille du texte des h2 pour 0.5em et tout en majuscule 

